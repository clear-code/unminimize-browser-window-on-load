/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import CrossSessionState from '/extlib/CrossSessionState.js';

import {
  log,
  configs,
  KEEP_TAB_MODE_NEW_TAB,
  KEEP_TAB_MODE_OLD_TAB,
} from './common.js';

chrome.tabs.onUpdated.addListener(async (id, updateInfo, tab) => {
  if (configs.keepTab != KEEP_TAB_MODE_NEW_TAB)
    return;

  let window = await chrome.windows.get(tab.windowId);
  if (window.state != 'minimized' &&
      window.focused)
    return;

  log('Tab in a background window is updated.', updateInfo);
  if (!('url' in updateInfo) &&
      !('status' in updateInfo))
    return;

  log('Page load on a background window is detected.');

  try {
    log('Trying to activate window via "chrome.windows.update()"...');
    await chrome.windows.update(tab.windowId, {
      focused: true,
      state:   'normal'
    });
  }
  catch(error) {
    log('Error:', error);
  }

  window = await chrome.windows.get(tab.windowId);
  if (window.state != 'minimized' &&
      window.focused) {
    log('Successfully activated.');
    return;
  }

  /*
  // fallback to another method
  try {
    log('Trying to restore window via "chrome.tabs.executeScript()"...');
    await chrome.tabs.executeScript(id, { code: `
      window.blur();
      setTimeout(() => window.focus(), 100);
    ` });
  }
  catch(error) {
    log('Error:', error);
  }

  window = await chrome.windows.get(tab.windowId);
  if (window.state != 'minimized' &&
      window.focused) {
    log('Successfully activated.');
    return;
  }
  */

  log('Failed to restore window.');
});

const gState = new CrossSessionState({
  windowByTab: {
    default: new Map(),
    set: value => [...value.entries()],
    get: value => new Map(value),
  },
  tabsByWindows: {
    default: new Map(),
    set: value => Array.from(value.entries(), ([windowId, tabIds]) => [windowId, [...tabIds]]),
    get: value => new Map(value.map(([windowId, tabIds]) => [windowId, new Set(tabIds)])),
  },
  lastFocusedWindowId: {
    default: -1,
  },
});

chrome.windows.onCreated.addListener(async window => {
  await gState.restored;
  const tabs = new Set();
  gState.tabsByWindows.set(window.id, tabs);
  if (window.tabs) {
    for (const tab of window.tabs) {
      tabs.add(tab.id);
      gState.windowByTab.set(tab.id, window.id);
    }
  }
  if (window.focused)
    gState.lastFocusedWindowId = window.id;
  gState.save('windowByTab', 'tabsByWindows');
});

chrome.windows.onRemoved.addListener(async windowId => {
  await gState.restored;
  const tabs = gState.tabsByWindows.get(windowId);
  for (const tabId of tabs) {
    gState.windowByTab.delete(tabId);
  }
  gState.tabsByWindows.delete(windowId);
  gState.save('windowByTab', 'tabsByWindows');
});

chrome.windows.onFocusChanged.addListener(async windowId => {
  await gState.restored;
  gState.lastFocusedWindowId = windowId;
});

chrome.tabs.onCreated.addListener(async tab => {
  await gState.restored;
  const tabs = gState.tabsByWindows.get(tab.windowId) || new Set();
  tabs.add(tab.id);
  gState.tabsByWindows.set(tab.windowId, tabs);
  gState.windowByTab.set(tab.id, tab.windowId);
  gState.save('windowByTab', 'tabsByWindows');
});

chrome.tabs.onRemoved.addListener(async (tabId, removeInfo) => {
  await gState.restored;
  gState.windowByTab.delete(tabId);
  const tabs = gState.tabsByWindows.get(removeInfo.windowId) || new Set();
  tabs.delete(tabId);
  if (removeInfo.isWindowClosing || tabs.size == 0)
    gState.tabsByWindows.delete(removeInfo.windowId);
  else
    gState.tabsByWindows.set(removeInfo.windowId, tabs);
  gState.save('windowByTab', 'tabsByWindows');
});

chrome.tabs.onAttached.addListener(async (tabId, attachInfo) => {
  await gState.restored;
  const tabs = gState.tabsByWindows.get(attachInfo.newWindowId) || new Set();
  tabs.add(tabId);
  gState.tabsByWindows.set(attachInfo.newWindowId, tabs);
  gState.windowByTab.set(tabId, attachInfo.newWindowId);
  gState.save('windowByTab', 'tabsByWindows');
});

chrome.tabs.onDetached.addListener(async (tabId, detachInfo) => {
  await gState.restored;
  const tabs = gState.tabsByWindows.get(detachInfo.oldWindowId) || new Set();
  tabs.delete(tabId);
  if (tabs.size == 0)
    gState.tabsByWindows.delete(detachInfo.oldWindowId);
  else
    gState.tabsByWindows.set(detachInfo.oldWindowId, tabs);
  gState.save('windowByTab', 'tabsByWindows');
});


chrome.webRequest.onBeforeRequest.addListener(details => {
  if (configs.keepTab != KEEP_TAB_MODE_OLD_TAB)
    return { cancel: false };

  const windowId = gState.windowByTab.get(details.tabId);
  if (gState.lastFocusedWindowId == windowId)
    return { cancel: false };

  log('Page request on a background window is detected.', windowId, details);

  log('Trying to activate window via "chrome.windows.update()"...');
  chrome.windows.update(windowId, {
    focused: true,
    state:   'normal'
  });
  return { cancel: true };
}, {
  urls:  ['<all_urls>'],
  types: ['main_frame']
}, ['blocking']);

gState.restored.then(async () => {
  if (gState.resumed) {
    log('resumed: skip initialization');
    return;
  }

  const tabs = await chrome.tabs.query({});

  for (const tab of tabs) {
    gState.windowByTab.set(tab.id, tab.windowId);
    const tabs = gState.tabsByWindows.get(tab.windowId) || new Set();
    tabs.add(tab.id);
    gState.tabsByWindows.set(tab.windowId, tabs);
  }
  log('windowByTab: ', gState.windowByTab);
  log('tabsByWindows: ', gState.tabsByWindows);
  gState.save('windowByTab', 'tabsByWindows');
});
