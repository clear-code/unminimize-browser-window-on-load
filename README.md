# unminimize-browser-window-on-load

In short: this introduces a behavior around minimized/background windows on the Internet Explorer.

This restores a minimized browser window when any page-load happens.
When a named browser window is opened via JavaScript code like `window.open("...", "subwindow", "width=400,height=400")` and the window is minimized, on Firefox the minimized window becomes an annoying trap. After that you may run the same JavaScript code to load any page into the named window, but Firefox doesn't restore the window if it is minimized. As the result, the URL specified to `window.open()` will be loaded in a minized window silently, and you'll see nothing happens even if you expect that the URL is loaded in any visible window.
This extension automatically restores the minimized named window on this scenario.
