---
CJKmainfont: Noto Sans CJK JP
CJKoptions:
  - BoldFont=Noto Sans CJK JP Bold
title: "Unminimize Browser Window On Load リリース前検証手順 v2.0"
author: "株式会社クリアコード"
date: "2024-06-10"
titlepage: true
colorlinks: true
toc-title: "目次"
toc-own-page: true
listings-disable-line-numbers: true
code-block-font-size: \footnotesize
footnotes-pretty: true
---

# 概要

本文書は、Unminimize Browser Window On Loadのリリースの際に実施する検証の手順を説明する物である。


# 検証環境の用意

検証には原則として `/doc/verify/windows-10-21H2` の環境を使用する。
他の環境を使用する場合は、以下の条件を事前に整えておく。

* Active Directoryドメイン参加状態である。
  （または、`/doc/verify/windows-10-21H2/ansible/files/join-dummy-domain.bat` の内容を管理者権限で実行済みである。）
* Google Chrome、Microsoft Edgeをインストール済みである。
* Google Chrome、Microsoft EdgeのGPO用ポリシーテンプレートを導入済みである。
* `C:\Users\Public\webextensions\unminimize-browser-window-on-load` 配下にこのリポジトリーの内容を配置済みである。
* `C:\Users\Public\webextensions\manifest.xml` の位置に `/doc/verify/windows-10-21H2/ansible/files/manifest.xml` を配置済みである。

準備は以下の手順で行う。

1. Chrome用アドオンの開発版パッケージを用意し、インストールするための設定を行う。
   1. Chromeを起動する。
   2. アドオンの管理画面（`chrome://extensions`）を開く。
   3. `デベロッパーモード` を有効化する。
   4. `拡張機能をパッケージ化` で `C:\Users\Public\webextensions\unminimize-browser-window-on-load` をパックする。（1つ上のディレクトリーに `unminimize-browser-window-on-load.crx` と `unminimize-browser-window-on-load.pem` が作られる）
   5. `chrome.crx` をChromeのアドオン管理画面にドラッグ＆ドロップし、インストールして、IDを控える。
      例：`egoppdngdcoikeknmbgiakconmchahhf`
   6. アドオンを一旦アンインストールする。
   7. `gpedit.msc` を起動する。
   8. `Computer Configuration\Administrative Templates\Google\Google Chrome\Extensions` （`コンピューターの構成\管理用テンプレート\Google\Google Chrome\拡張機能`）を開いて、以下のポリシーを設定する。
      * `Configure the list of force-installed apps and extensions`（`自動インストールするアプリと拡張機能のリストの設定`）
        * `Enabled`（`有効`）に設定して、`Show...`（`表示...`）をクリックし、以下の項目を追加する。
          * `<先程控えたID>;file:///C:/Users/Public/webextensions/manifest.xml`
            例：`egoppdngdcoikeknmbgiakconmchahhf;file:///C:/Users/Public/webextensions/manifest.xml`
   9. Chromeを再起動し、アドオンの管理画面（`chrome://extensions`）を開いて、Unminimize Browser Window On Loadの開発版が管理者によってインストールされた状態になっていることを確認する。
2. 開発版パッケージをEdgeアドオンとしてインストールするための設定を行う。
   1. `C:\Users\Public\webextensions\manifest.xml` のEdge用アドオンのIDを、先程控えたIDで置き換える。
   2. `gpedit.msc` を起動する。
   3. `Computer Configuration\Administrative Templates\Microsoft Edge\Extensions`（`コンピューターの構成\管理用テンプレート\Microsoft Edge\拡張機能`）を開いて、以下のポリシーを設定する。
      * `Control which extensions are installed silently`（`サイレント インストールされる拡張機能を制御する`）
        * `Enabled`（`有効`）に設定して、`Show...`（`表示...`）をクリックし、以下の項目を追加する。
          * `<先程控えたID>;file:///C:/Users/Public/webextensions/manifest.xml`
            例：`oapdkmbdgdcjpacbjpcdfhncifimimcj;file:///C:/Users/Public/webextensions/manifest.xml`
   4. Edgeを再起動し、アドオンの管理画面（`edge://extensions`）を開いて、Unminimize Browser Window On Loadの開発版が管理者によってインストールされた状態になっていることを確認する。

# 検証

## 基本動作

### 検証

1. Edgeを起動する。
2. testcase.htmlを開く。
3. ファイル内の指示に従って操作する。
   * 期待される結果：
     * ファイル内に記載の通りの結果を得られる。
